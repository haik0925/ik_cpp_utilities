#ifndef IK_DYNAMIC_ARRAY_H
#define IK_DYNAMIC_ARRAY_H
#include <stdlib.h>

#define MALLOC(size) malloc(size)
#define FREE(ptr) free(ptr)

template <typename T>
struct ik_DynamicArray
{
    T* data;
    int count;
    int reserved_count;
};

template <typename T>
void ikda_init(ik_DynamicArray<T>* da, int reserved_count = 0)
{
    da->data = (T*)MALLOC(sizeof(T) * reserved_count);
    da->count = 0;
    da->reserved_count = reserved_count;
}

template <typename T>
void ikda_free(ik_DynamicArray<T>* da)
{
    free(da->data);
    *da = {};
}

template <typename T>
void ikda_push(ik_DynamicArray<T>* da, const T& value)
{
    if (da->count >= da->reserved_count)
        ikda__grow_buffer(da);
    da->data[da->count++] = value;
}

template <typename T>
void ikda_pop(ik_DynamicArray<T>* da)
{
    --da->count;
}

template <typename T>
void ikda__grow_buffer(ik_DynamicArray<T>* da)
{
    if (da->reserved_count > 0)
        da->reserved_count *= 2;
    else
        da->reserved_count = 1;

    T* new_data = (T*)MALLOC(sizeof(T) * da->reserved_count);
    ikda__copy(da->data, new_data, da->count);
    FREE(da->data);
    da->data = new_data;
}

template <typename T>
void ikda__copy(T* src, T* dest, int count)
{
    for (int i = 0; i < count; ++i)
        dest[i] = src[i];
}

#endif//IK_DYNAMIC_ARRAY_H
